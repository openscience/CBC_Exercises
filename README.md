CBC Exercises

This repository includes files for introductory exercises for students learning about LIGO, Compact Binary Coalescence, orbital dynamics, GW waveforms, SNR, detection horizon and range, etc.

Slides from the lectures: - LIGO_CBC_Les_Houches_20180712.pdf

Please also see the tutorials (and slides and videos of the talks) at our LIGO-Virgo Open Data Workshop from March 2018 - https://losc.ligo.org/s/workshop1/course.html . You are encouraged to work through all the tutorials on that site.

Meilleures salutations, 
Alan Weinstein , ajw@ligo.caltech.edu 
